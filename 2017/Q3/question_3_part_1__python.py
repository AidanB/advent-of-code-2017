#unfinished
import math
input = 368078

#first calculate the nearest higher odd perfect square, which will always have a manhatten path of length n-1 where n is its sqrt
i = 1
while i*i <= input:
    i = i+2
square = i*i
step = i-1

#calculate corners of the ring that the input is on
pivots = [square,square-step,square-step*2,square-step*3]

#get distance from nearest corner
j = square
distFromPivot = 0
while j > input:
    if j in pivots:
        distFromPivot = 0
    else:
        distFromPivot = distFromPivot + 1
    j = j-1
    
distFromPivot = distFromPivot +1
if distFromPivot > step/2:
    distFromPivot = distFromPivot - (distFromPivot-step/2)*2

#this is our answer
print(step-distFromPivot)